import 'dart:convert';
import 'dart:io';

import 'package:dermits/Model/User.dart';
import 'package:dermits/global_var.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';

class UserHelper {
  Future<User> getUser(String username) async {
    final response =
        await http.post("${link}get_user.php", body: {"user_id": username});

    if (response.statusCode == 200) {
      if (response.body != null) {
        return User.fromJson(json.decode(response.body));
      }

      throw Exception("user not found !");
    } else {
      throw Exception("Something went wrong");
    }
  }

  Future<bool> changePassword(
      String oldPassword, String newPassword, int userId) async {
    final response = await http.post(
      "${link}change_password.php",
      body: {
        "new_password": newPassword,
        "old_password": oldPassword,
        "user_id": userId.toString()
      },
    );

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Something went wrong");
    }
  }

  Future<bool> changeData(
    int userId,
    String password,
    String name,
    String email,
    String phone,
    String username,
    String dob,
  ) async {
    final response = await http.post(
      "${link}change_data.php",
      body: {
        "user_id": userId.toString(),
        "password": password,
        "name": name,
        "email": email,
        "phone": phone,
        "username": username,
        "dob": dob,
      },
    );

    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("something went wrong...");
    }
  }

  Future<bool> changeProfile(File _image, int userId, String password) async {
    // create multipart request
    var request = new http.MultipartRequest(
        "POST", Uri.parse("${link}change_displaypict.php"));

    // if you need more parameters to parse, add those like this. i added "user_id". here this "user_id" is a key of the API request
    request.fields["user_id"] = userId.toString();
    request.fields["password"] = password;

/*     var multipartFile = http.MultipartFile.fromBytes(
      'displaypic',
      await _image.readAsBytes(),
    ); */

    var multipartFile = await http.MultipartFile.fromPath(
        "displaypic", _image.path,
        filename: "1.jpeg", contentType: MediaType("image", "jpeg"));

    // add file to multipart
    request.files.add(multipartFile);

    // send request to upload image
    bool hasil = await request.send().then((response) {
      // listen for response
      response.stream.transform(utf8.decoder).listen((value) {
        print(value);
      });

      if (response.statusCode == 200) {
        return true;
      } else {
        throw Exception("Something went wrong");
      }
      // listen for response
    }).catchError((e) {
      print(e);
      return false;
    });

    return hasil;
  }
}
