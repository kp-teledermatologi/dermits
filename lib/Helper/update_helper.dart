import 'dart:convert';

import 'package:dermits/Model/Diagnosis.dart';
import 'package:dermits/global_var.dart';
import 'package:http/http.dart' as http;

class UpdateHelper {
  Future<List<Diagnosis>> getKeluhan(int userId) async {
    final response = await http.post("${link}update_keluhan.php",
        body: {"user_id": userId.toString()});

    if (response.statusCode == 200) {
      if (response.body != null) {
        List newData = json.decode(response.body);

        return newData.map((e) => Diagnosis.fromJson(e)).toList();
      }
    }

    return [];
  }

  Future<List<Diagnosis>> getDiagnosa(String username) async {
    print(username);
    final response = await http
        .post(link + "update_diagnosa.php", body: {"username": username});

    print(response.statusCode);

    if (response.statusCode == 200) {
      if (response.body != null) {
        List newData = json.decode(response.body);

        return newData.map((e) => Diagnosis.fromJson(e)).toList();
      }
    }
  }
}
