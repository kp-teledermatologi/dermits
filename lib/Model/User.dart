class User {
  int idUser;
  String email;
  String name;
  String profilePicture;
  String phone;
  String hospital;
  String username;
  String dob;

  User(
      {this.email,
      this.name,
      this.profilePicture,
      this.phone,
      this.hospital,
      this.username,
      this.dob,
      this.idUser});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        idUser: int.parse(json['id_user']),
        email: json['email'],
        name: json['name'],
        profilePicture: json['profilePicture'],
        phone: json['phone'],
        hospital: json['hospital'],
        username: json['username'],
        dob: json['dob']);
  }
}
