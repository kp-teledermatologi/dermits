import 'package:dermits/Model/User.dart';

class Diagnosis {
  int idDiagnosis;
  String disease;
  String description;
  String imageCamera;
  String keluhan;
  User user;

  Diagnosis(
      {this.idDiagnosis,
      this.disease,
      this.description,
      this.keluhan,
      this.imageCamera,
      this.user});

  factory Diagnosis.fromJson(Map<String, dynamic> json) {
    return Diagnosis(
      idDiagnosis: int.parse(json['id_examinations']),
      disease: json['diagnoses_disease'],
      description: json['diagnoses_description'],
      keluhan: json['keluhan'],
      imageCamera: json['images_camera'],
      user: User(
          name: json['name'],
          hospital: json['hospital'],
          profilePicture: json['profilePicture']),
    );
  }
}
