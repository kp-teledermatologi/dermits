import 'package:dermits/akun/ubah_data.dart';
import 'package:dermits/akun/ubah_foto.dart';
import 'package:dermits/akun/ubah_password.dart';
import 'package:dermits/chatting.dart';
import 'package:dermits/daftar_chat.dart';
import 'package:dermits/homepage_user.dart';
import 'package:dermits/isi_diagnosa.dart';
import 'package:dermits/rootPage.dart';
import 'package:dermits/splashscreen.dart';
import 'package:flutter/material.dart';
import 'package:dermits/upload_keluhan.dart';
import 'package:dermits/lihat_diagnosa.dart';
import 'package:dermits/riwayat_keluhan.dart';
import 'package:dermits/login.dart';
import 'package:dermits/SignUp.dart';
import 'package:flutter/services.dart';
import 'package:dermits/Isi_profil.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Color.fromRGBO(0, 0, 0, 0)));
    return MaterialApp(
      title: 'DermITS',
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primaryIconTheme:
              IconThemeData(color: Colors.white), //appbar icon color
          primaryTextTheme: TextTheme(
            headline6: TextStyle(color: Colors.white),
          ), //appbar text color
          primaryColor: Color.fromRGBO(15, 28, 72, 1),
          accentColor: Color(0xFF2B709D),
          appBarTheme: AppBarTheme(brightness: Brightness.dark),
          buttonTheme: ButtonThemeData(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
            buttonColor: Color.fromRGBO(43, 112, 157, 1),
            textTheme: ButtonTextTheme.primary,
          ),
          inputDecorationTheme: InputDecorationTheme(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          )),
      home: SplashScreenPage(),
      routes: {
        '/login': (context) => Login(),
        '/root_page': (context) => RootPage(),
        '/ubah_password': (context) => UbahPassword(),
        '/ubah_data': (context) => UbahData(),
        '/ubah_foto': (contet) => UbahFoto(),
        '/riwayat_keluhan': (context) => RiwayatKeluhan(),
        '/lihat_diagnosa': (context) => LihatDiagnosa(),
        '/isi_diagnosa': (context) => IsiDiagnosa(),
        '/upload_keluhan': (context) => UploadKeluhan(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
