import 'package:dermits/Helper/update_helper.dart';
import 'package:dermits/Model/Diagnosis.dart';
import 'package:dermits/component/loading.dart';
import 'package:dermits/component/update_card_dokter.dart';
import 'package:dermits/global_var.dart';
import 'package:flutter/material.dart';

class HomepageDokter extends StatefulWidget {
  HomepageDokter({Key key, this.toChat}) : super(key: key);

  final VoidCallback toChat;

  @override
  _HomepageDokterState createState() => _HomepageDokterState();
}

class _HomepageDokterState extends State<HomepageDokter> {
  final UpdateHelper _helper = UpdateHelper();

  List<Diagnosis> _keluhan = [];
  Loading _load;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      _getData();
    });
  }

  void _getData() async {
    _load.show();

    List<Diagnosis> _tempData = await _helper.getDiagnosa(username);

    setState(() {
      _keluhan = _tempData;
    });

    _load.hide();
  }

  @override
  Widget build(BuildContext context) {
    _load = Loading(context: context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Halo, $username"),
      ),
      body: SafeArea(
        minimum: EdgeInsets.symmetric(horizontal: 8),
        child: Column(
          children: [
            SizedBox(height: 20),
            Card(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * .4,
                      child: MaterialButton(
                        onPressed: widget.toChat,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.chat,
                              color: Theme.of(context).accentColor,
                            ),
                            SizedBox(height: 8),
                            FittedBox(
                              child: Text(
                                "Lihat Chat",
                                style: TextStyle(
                                    color: Theme.of(context).accentColor),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 30,
                      width: 1,
                      color: Colors.grey,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * .4,
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/riwayat_keluhan');
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.history,
                              color: Theme.of(context).accentColor,
                            ),
                            SizedBox(height: 8),
                            FittedBox(
                              child: Text(
                                "Riwayat Diagnosa",
                                style: TextStyle(
                                    color: Theme.of(context).accentColor),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 10),
            Row(
              children: [
                FittedBox(
                    child: Text(
                  "Update",
                  style: TextStyle(color: Colors.grey),
                )),
                Container(
                  margin: EdgeInsets.only(left: 10),
                  width: MediaQuery.of(context).size.width * .7,
                  height: 1,
                  color: Colors.grey,
                )
              ],
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemBuilder: (context, index) => UpdateCardDokter(
                  diagnosis: _keluhan[index],
                ),
                itemCount: _keluhan.length,
              ),
            )
          ],
        ),
      ),
    );
  }
}
