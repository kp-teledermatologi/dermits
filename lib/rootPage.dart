import 'package:dermits/akun/mnjmn_akun.dart';
import 'package:dermits/chatting.dart';
import 'package:dermits/daftar_chat.dart';
import 'package:dermits/global_var.dart';
import 'package:dermits/homepage_dokter.dart';
import 'package:dermits/homepage_user.dart';
import 'package:flutter/material.dart';

import 'isi_diagnosa.dart';
import 'riwayat_keluhan.dart';

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    HomepageUser(), //not used
    DaftarChat(),
    ManajemenAkun()
  ];

  void toChat() {
    setState(() {
      _currentIndex = 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /**
       * jika index berada di 0, dan role bukan pasien return homepageDokter
       * else return homepageUser
       */
      body: _currentIndex != 0
          ? _children[_currentIndex]
          : role != '0'
              ? HomepageDokter(
                  toChat: toChat,
                )
              : _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: (int index) {
          setState(() {
            _currentIndex = index;
          });
        },
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text("Home")),
          BottomNavigationBarItem(icon: Icon(Icons.chat), title: Text("Chat")),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_circle), title: Text("Account")),
        ],
      ),
    );
  }
}
