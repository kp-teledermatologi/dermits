import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:dermits/global_var.dart';
import 'dart:io';
import 'package:dermits/component/loading.dart';
import 'package:dermits/chatting.dart';

class LihatDiagnosa extends StatefulWidget {
  @override
  _LihatDiagnosa createState() => _LihatDiagnosa();
}

class _LihatDiagnosa extends State<LihatDiagnosa> {
  Loading _load;

  @override
  void initState() { 
    super.initState();
    Future.delayed(Duration.zero, () {
      ambilDataDiagnosa();
    });
  }

  List dataDiagnosa = [
    {"checked": "0"}
  ];
  List daftarNamaObat = [];
  List daftarDosisObat = [];

  bool adaDiagnosa = true;
  bool adaResepObat = true;

  void ambilDataDiagnosa() async {
    _load.show();
    print("TEST ${pilihIdExamination}");
    var url = "${link}lihat_diagnosa.php";
    var response = await http.post(url, body: {
      "id_examinations": "${pilihIdExamination}",
    });
    setState(() {
      dataDiagnosa = json.decode(response.body);
    });

    print("daftar ${dataDiagnosa[0]["checked"]}");
    if (dataDiagnosa[0]["diagnoses_recipe_type_medicine"] != "null") {
      print("coba");
      daftarNamaObat =
          "${dataDiagnosa[0]["diagnoses_recipe_type_medicine"]}".split(', ');
      daftarDosisObat =
          "${dataDiagnosa[0]["diagnoses_recipe_type_usage"]}".split(', ');
    }

    print("haloo : ${daftarNamaObat.length}");
    if (daftarNamaObat[0] == "" || daftarNamaObat[0] == "null") {
      print("test");
      setState(() {
        daftarNamaObat = [];
      });
    }
    _load.hide();
  }

  Widget fieldLihatObat(BuildContext context, int index) {
    return Card(
        child: ListTile(
      leading: CircleAvatar(
        backgroundImage: AssetImage("img/obat.png"),
      ),
      title: Text("Obat ${index + 1}"),
      subtitle: Text("${daftarNamaObat[index]} (${daftarDosisObat[index]})"),
    ));
  }

  Widget statusDiagnosa() {
    var statusDiagnosa0 = RichText(
      text: TextSpan(
        style: TextStyle(fontSize: 18),
        children: <TextSpan>[
          TextSpan(text: 'Status: '),
          TextSpan(
              text: 'Menunggu Diagnosa Klinik',
              style: TextStyle(color: Colors.yellow)),
        ],
      ),
    );

    var statusDiagnosa1 = RichText(
      text: TextSpan(
        style: TextStyle(fontSize: 18),
        children: <TextSpan>[
          TextSpan(text: 'Status: '),
          TextSpan(
              text: 'Sudah Didiagnosa Klinik',
              style: TextStyle(color: Colors.greenAccent[400])),
        ],
      ),
    );

    var statusDiagnosa2 = RichText(
      text: TextSpan(
        style: TextStyle(fontSize: 18),
        children: <TextSpan>[
          TextSpan(text: 'Status: '),
          TextSpan(
              text: 'Menunggu Diagnosa RS',
              style: TextStyle(color: Colors.yellow)),
        ],
      ),
    );

    var statusDiagnosa3 = RichText(
      text: TextSpan(
        style: TextStyle(fontSize: 18),
        children: <TextSpan>[
          TextSpan(text: 'Status: '),
          TextSpan(
              text: 'Sudah Didiagnosa RS',
              style: TextStyle(color: Colors.greenAccent[400])),
        ],
      ),
    );

    if (dataDiagnosa[0]["checked"] == "0") {
      return Container(
        padding: const EdgeInsets.all(15.0),
        color: Color.fromRGBO(43, 112, 157, 1),
        child: statusDiagnosa0,
      );
    } else if (dataDiagnosa[0]["checked"] == "1") {
      return Container(
        padding: const EdgeInsets.all(15.0),
        color: Color.fromRGBO(43, 112, 157, 1),
        child: statusDiagnosa1,
      );
    } else if (dataDiagnosa[0]["checked"] == "2") {
      return Container(
        padding: const EdgeInsets.all(15.0),
        color: Color.fromRGBO(43, 112, 157, 1),
        child: statusDiagnosa2,
      );
    } else if (dataDiagnosa[0]["checked"] == "3") {
      return Container(
        padding: const EdgeInsets.all(15.0),
        color: Color.fromRGBO(43, 112, 157, 1),
        child: statusDiagnosa3,
      );
    }
  }

  Widget subjudulDiagnosa() {
    if (dataDiagnosa[0]["diagnoses_description"] != null && dataDiagnosa[0]["diagnoses_disease"] != null) {
      adaDiagnosa = true;
      return Text(
        "Diagnosa",
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
      );
    } 
    else {
      adaDiagnosa = false;
      return Container(color: Colors.white);
    }
  }

  Widget deskripsiDiagnosa() {
    var teksDiagnosa = RichText(
      text: TextSpan(
        style: TextStyle(color: Colors.black),
        children: <TextSpan>[
          TextSpan(
              text: '${dataDiagnosa[0]["diagnoses_disease"]}\n',
              style: TextStyle(fontWeight: FontWeight.bold)),
          TextSpan(text: '${dataDiagnosa[0]["diagnoses_description"]}'),
        ],
      ),
    );

    if (dataDiagnosa[0]["diagnoses_description"] != "" && dataDiagnosa[0]["diagnoses_description"] != null) {
      return Container(
        padding: const EdgeInsets.all(15.0),
        margin: const EdgeInsets.only(left: 20.0, right: 20.0),
        decoration: BoxDecoration(
            color: Color.fromRGBO(233, 232, 232, 1),
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(10.0)),
        child: teksDiagnosa,
      );
    } 
    else {
      return Container(color: Colors.white);
    }
  }

  Widget subjudulResepObat() {
    if (dataDiagnosa[0]["diagnoses_recipe_type_medicine"] != "" && dataDiagnosa[0]["diagnoses_recipe_type_medicine"] != null) {
      return Text(
        "Resep Obat",
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
      );
    } else {
      adaResepObat = false;
      return Container(color: Colors.white);
    }
  }

  @override
  Widget build(BuildContext context) {
    _load = Loading(context: context);
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Lihat Diagnosa"),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          ),
        ),
        body: ListView(
            padding: EdgeInsets.only(left: 20, right: 20),
            children: <Widget>[
              Column(children: <Widget>[
                SizedBox(height: 30),
                statusDiagnosa(),
                SizedBox(height: 30),
                Image.network('$link${dataDiagnosa[0]["images_camera"]}'),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.person,
                            color: Color.fromRGBO(43, 112, 157, 1)),
                        onPressed: null,
                        iconSize: 50),
                    Flexible(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                          Text("Pasien: ${dataDiagnosa[0]["patient_name"]}",
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18)),
                          Text("Dokter: ${dataDiagnosa[0]["doctor_name"]}",
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18)),
                        ]))
                  ],
                ),
                SizedBox(height: 35),
                Text(
                  "Keluhan",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                SizedBox(height: 5),
                Container(
                  padding: const EdgeInsets.all(15.0),
                  margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(233, 232, 232, 1),
                      border: Border.all(
                        color: Colors.grey,
                      ),
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Text("${dataDiagnosa[0]["keluhan"]}"),
                ),
                SizedBox(height: 30),
                subjudulDiagnosa(),
                adaDiagnosa ? SizedBox(height: 5) : Container(),
                deskripsiDiagnosa(),
                adaDiagnosa ? SizedBox(height: 30) : Container(),
                subjudulResepObat(),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: daftarNamaObat.length,
                    itemBuilder: (BuildContext context, int index) =>
                        fieldLihatObat(context, index)),
                SizedBox(height: 15),
                RaisedButton(
                  onPressed: () {
                    role == "0" ? id_penerima = "${dataDiagnosa[0]["id_user"]}" : id_penerima = "${dataDiagnosa[0]["patient_code"]}";
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => ChatScreen())
                    );
                  },
                  color: Color.fromRGBO(43, 112, 157, 1),
                  child: const Text('Chat',
                      style: TextStyle(color: Colors.white)),
                ),
                SizedBox(height: 15),
              ])
            ]));
  }
}
