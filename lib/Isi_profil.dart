import 'package:flutter/material.dart';
import 'package:dermits/SignUp.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:dermits/global_var.dart';
import 'dart:convert';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:dermits/homepage_user.dart';

class IsiProfil extends StatefulWidget {
  @override
  _IsiProfil createState() => _IsiProfil();
}

class _IsiProfil extends State<IsiProfil> {
  DateTime selectedDate = DateTime.now();
  int _radioVal = 0;
  File _image;
  bool adaFoto = false;
  final picker = ImagePicker();

  final _key = new GlobalKey<FormState>();
  final namaLengkap = TextEditingController();
  final phone = TextEditingController();
  final alamat = TextEditingController();
  final noIdentitas = TextEditingController();

  Future getImageCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    setState(() {
      _image = File(pickedFile.path);
      adaFoto = true;
    });
  }

  Future<Null> _selectedDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: new DateTime(1950),
        lastDate: DateTime(selectedDate.year + 1));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  Future getImageGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      _image = File(pickedFile.path);
      adaFoto = true;
    });
  }

  Widget tampilkanFoto() {
    if (adaFoto == false) {
      return CircleAvatar(
        backgroundImage: AssetImage("img/user.png"),
        minRadius: 120,
        maxRadius: 120,
      );
    } else {
      return CircleAvatar(
        backgroundImage: FileImage(_image),
        minRadius: 120,
        maxRadius: 120,
      );
    }
  }

  kirimData() async {
    var base64img = base64Encode(_image.readAsBytesSync());
    String tanggal = DateFormat('dd-MM-yyyy').format(selectedDate);
    var usernameBaru = usernamenew.text;
    var fileName = '${usernameBaru}_${tanggal}.jpg';
    var url = link + "register.php";
    var response = await http.post(url, body: {
      "username": usernamenew.text,
      "email": emailnew.text,
      "password": passwordnew.text,
      "image": base64img,
      "images_camera": fileName,
      "namalengkap": namaLengkap.text,
      "gender": _radioVal.toString(),
      "phone": phone.text,
      "dob": selectedDate.toString(),
      "role": "0",
      "alamat": alamat.text,
      "no_identitas": noIdentitas.text,
    });
    //print(response.body);
    final respon = jsonDecode(response.body);
    int value = int.parse(respon['value']);
    if (value == 1) {
      Fluttertoast.showToast(msg: "Data Berhasil Disimpan");
      usernameBaru = usernameBaru;
      role = "0";
      id_user = respon['id_user'].toString();
      name = namaLengkap.text;

      Navigator.pushReplacementNamed(context, "/root_page");
      print("bisa");
    } else if (value == 0) {
      Fluttertoast.showToast(msg: "Gagal Menyimpan Data");
      print("gagal");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Lengkapi Profil Anda"),
          backgroundColor: Color.fromRGBO(15, 28, 72, 1),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          ),
        ),
        body:
            ListView(padding: EdgeInsets.only(left: 30, right: 30), children: <
                Widget>[
          Column(children: <Widget>[
            SizedBox(height: 30),
            tampilkanFoto(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                IconButton(
                    icon: Icon(Icons.camera_alt,
                        color: Color.fromRGBO(43, 112, 157, 1)),
                    onPressed: () {
                      getImageCamera();
                    },
                    iconSize: 40),
                IconButton(
                    icon: Icon(Icons.image,
                        color: Color.fromRGBO(43, 112, 157, 1)),
                    onPressed: () {
                      getImageGallery();
                    },
                    iconSize: 40),
              ],
            ),
            SizedBox(height: 10),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  //key: _key,
                  autocorrect: false,
                  controller: namaLengkap,
                  decoration: InputDecoration(
                    hintText: 'Nama Lengkap',
                    hintStyle: new TextStyle(
                        color: Color.fromRGBO(43, 112, 157, 1), fontSize: 16.0),
                    filled: true,
                    fillColor: Color.fromRGBO(233, 232, 232, 1),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 10),
            Text(
              "Tanggal Lahir :",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Color.fromRGBO(43, 112, 157, 1)),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  "${selectedDate.toLocal()}".split(' ')[0],
                  style: TextStyle(
                    fontSize: 18,
                    color: Color.fromRGBO(43, 112, 157, 1),
                  ),
                ),
                RaisedButton(
                  color: Color.fromRGBO(43, 112, 157, 1),
                  onPressed: () => _selectedDate(context),
                  child: const Text('Atur Tanggal',
                      style: TextStyle(fontSize: 15, color: Colors.white)),
                ),
              ],
            ),
            SizedBox(height: 20),
            Text(
              "Jenis Kelamin :",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Color.fromRGBO(43, 112, 157, 1)),
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [0, 1]
                  .map((int index) => Radio<int>(
                        value: index,
                        activeColor: Color.fromRGBO(43, 112, 157, 1),
                        groupValue: this._radioVal,
                        onChanged: (int value) {
                          setState(() => this._radioVal = value);
                        },
                      ))
                  .toList(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  "Pria",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Color.fromRGBO(43, 112, 157, 1)),
                ),
                Text(
                  "Wanita",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Color.fromRGBO(43, 112, 157, 1)),
                ),
              ],
            ),
            SizedBox(height: 10),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  autocorrect: false,
                  controller: phone,
                  decoration: InputDecoration(
                    hintText: 'No. Telepon',
                    hintStyle: new TextStyle(
                        color: Color.fromRGBO(43, 112, 157, 1), fontSize: 16.0),
                    filled: true,
                    fillColor: Color.fromRGBO(233, 232, 232, 1),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 10),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  autocorrect: false,
                  controller: alamat,
                  decoration: InputDecoration(
                    hintText: 'Alamat',
                    hintStyle: new TextStyle(
                        color: Color.fromRGBO(43, 112, 157, 1), fontSize: 16.0),
                    filled: true,
                    fillColor: Color.fromRGBO(233, 232, 232, 1),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 10),
            Container(
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextFormField(
                  autocorrect: false,
                  controller: noIdentitas,
                  decoration: InputDecoration(
                    hintText: 'No. Identitas',
                    hintStyle: new TextStyle(
                        color: Color.fromRGBO(43, 112, 157, 1), fontSize: 16.0),
                    filled: true,
                    fillColor: Color.fromRGBO(233, 232, 232, 1),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide:
                          BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                    ),
                  ),
                ),
              ),
            ),
            RaisedButton(
              onPressed: () {
                kirimData();
              },
              color: Color.fromRGBO(43, 112, 157, 1),
              child: const Text('Simpan',
                  style: TextStyle(fontSize: 20, color: Colors.white)),
            ),
            SizedBox(height: 15),
          ])
        ]));
  }
}
