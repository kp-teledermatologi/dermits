import 'package:dermits/Isi_profil.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:dermits/global_var.dart';
import 'dart:convert';
import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUp createState() => _SignUp();
}

final usernamenew = TextEditingController();
final emailnew = TextEditingController();
final passwordnew = TextEditingController();
final konfirmasi = TextEditingController();

class _SignUp extends State<SignUp> {
  bool _secureText = true;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: Color.fromRGBO(251, 120, 19, 1),
        body: ListView(
            padding: EdgeInsets.only(left: 60, right: 60),
            children: <Widget>[
          Column(children: <Widget>[
            SizedBox(height: 150),
            Image(image: AssetImage("img/logo.png")),
            SizedBox(
              height: 70,
            ),
            Container(
              child: Form(
                  child: Column(
                children: <Widget>[
                  TextField(
                    controller: usernamenew,
                    decoration: InputDecoration(
                      hintText: 'Username',
                      hintStyle: new TextStyle(
                          color: Color.fromRGBO(43, 112, 157, 1),
                          fontSize: 16.0),
                      filled: true,
                      fillColor: Color.fromRGBO(233, 232, 232, 1),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                    ),
                  ),
                  SizedBox(height: 15),
                  TextField(
                    controller: emailnew,
                    decoration: InputDecoration(
                      hintText: 'Email',
                      hintStyle: new TextStyle(
                          color: Color.fromRGBO(43, 112, 157, 1),
                          fontSize: 16.0),
                      filled: true,
                      fillColor: Color.fromRGBO(233, 232, 232, 1),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                    ),
                  ),
                  SizedBox(height: 15),
                  TextField(
                    controller: passwordnew,
                    obscureText: _secureText,
                    decoration: InputDecoration(
                      hintText: 'Password',
                      suffixIcon: IconButton(
                        onPressed: showHide,
                        icon: Icon(_secureText
                            ? Icons.visibility_off
                            : Icons.visibility),
                      ),
                      hintStyle: new TextStyle(
                          color: Color.fromRGBO(43, 112, 157, 1),
                          fontSize: 16.0),
                      filled: true,
                      fillColor: Color.fromRGBO(233, 232, 232, 1),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextField(
                    controller: konfirmasi,
                    obscureText: _secureText,
                    decoration: InputDecoration(
                      hintText: 'Konfirmasi Password',
                      suffixIcon: IconButton(
                        onPressed: showHide,
                        icon: Icon(_secureText
                            ? Icons.visibility_off
                            : Icons.visibility),
                      ),
                      hintStyle: new TextStyle(
                          color: Color.fromRGBO(43, 112, 157, 1),
                          fontSize: 16.0),
                      filled: true,
                      fillColor: Color.fromRGBO(233, 232, 232, 1),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                    ),
                  ),
                ],
              )),
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  onPressed: () {
                    if (passwordnew.text != konfirmasi.text) {
                      Fluttertoast.showToast(msg: "Konfirmasi Password Salah");
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => SignUp()));
                    } else if (passwordnew.text == konfirmasi.text) {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => IsiProfil()));
                    }
                  },
                  color: Color.fromRGBO(43, 112, 157, 1),
                  child: const Text('Sign Up',
                      style: TextStyle(fontSize: 20, color: Colors.white)),
                ),
              ],
            ),
            SizedBox(height: 15),
          ])
        ]));
  }
}
