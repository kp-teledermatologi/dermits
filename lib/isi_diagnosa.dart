import 'package:dermits/rootPage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:dermits/global_var.dart';
import 'dart:convert';
import 'package:dermits/chatting.dart';
import 'package:dermits/component/loading.dart';

var countResep;

class IsiDiagnosa extends StatefulWidget {
  @override
  _IsiDiagnosa createState() => _IsiDiagnosa();
}

class _IsiDiagnosa extends State<IsiDiagnosa> {
  Loading _load;

  @override
  void initState() {
    countResep = 0;
    super.initState();
    Future.delayed(Duration.zero, () {
      ambilDataDiagnosa();
    });
  }

  final controllerDiagnosa = TextEditingController();
  final controllerNamaPenyakit = TextEditingController();
  var controllerNamaObat = <TextEditingController>[];
  var controllerDosisObat = <TextEditingController>[];
  List dataDiagnosa = [
    {"checked": "0"}
  ];
  var respon;
  var namaObat = '';
  var dosisObat = '';
  bool adaDiagnosa = false;

  void ambilDataDiagnosa() async {
    _load.show();
    var url = "${link}lihat_diagnosa.php";
    var response = await http.post(url, body: {
      "id_examinations": "$pilihIdExamination",
    });
    setState(() {
      dataDiagnosa = json.decode(response.body);
    });
    _load.hide();
  }

  void kirimRujukan() async {
    var url = "${link}rujuk_rs.php";
    var response =
        await http.post(url, body: {"id_examinations": '$pilihIdExamination'});
    respon = jsonDecode(response.body);
    if (respon["value"] == '1') {
      popupBerhasilRujuk(context);
    } else if (respon["value"] == '0') {
      popupGagalRujuk(context);
    }
  }

  void kirimDiagnosa() async {
    if(adaDiagnosa) {
      for (int i = 0; i < controllerNamaObat.length; i++) {
        if (i == 0) {
          namaObat = controllerNamaObat[i].text;
          dosisObat = controllerDosisObat[i].text;
        } else {
          namaObat = '$namaObat, ${controllerNamaObat[i].text}';
          dosisObat = '${dosisObat}, ${controllerDosisObat[i].text}';
        }
      }

      var url = "${link}isi_diagnosa.php";
      var response = await http.post(url, body: {
        "id_examinations": '${pilihIdExamination}',
        "checked": '${int.parse(dataDiagnosa[0]["checked"]) + 1}',
        "diagnoses_description": controllerDiagnosa.text,
        "diagnoses_disease": controllerNamaPenyakit.text,
        "diagnoses_recipe_type_medicine": namaObat,
        "diagnoses_recipe_type_usage": dosisObat,
      });
      respon = jsonDecode(response.body);
      if (respon["value"] == '1') {
        popupBerhasil(context);
      } else if (respon["value"] == '0') {
        popupGagal(context);
      }
    }
    else{
      popupTeksKosong(context);
    }
  }

  Future<void> popupBerhasil(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Isi Diagnosa Berhasil'),
          content: const Text('Data anda telah masuk ke database.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RootPage()),
                );
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> popupGagal(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Isi Diagnosa Gagal'),
          content: const Text('Data tidak terkirim ke database.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RootPage()),
                );
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> popupBerhasilRujuk(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Rujuk ke RS Berhasil'),
          content: const Text('Data akan masuk ke dokter RS.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RootPage()),
                );
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> popupGagalRujuk(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Rujuk ke RS Gagal'),
          content: const Text('Data belum terkirim ke database RS.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RootPage()),
                );
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> popupTeksKosong(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Isi Diagnosa Gagal'),
          content: const Text('Teks diagnosa belum diisi secara lengkap.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget fieldIsiObat(int indexObat) {
    indexObat += 1;
    return Column(children: <Widget>[
      Text("Obat $indexObat"),
      SizedBox(height: 5),
      TextField(
          controller: controllerNamaObat[indexObat - 1],
          decoration: InputDecoration(hintText: "Nama Obat")),
      SizedBox(height: 5),
      TextField(
          controller: controllerDosisObat[indexObat - 1],
          decoration: InputDecoration(hintText: "Dosis Obat")),
      SizedBox(height: 20),
    ]);
  }

  Widget buttonTambahKurangObat(int indexObat) {
    if (indexObat < 1) {
      return FloatingActionButton(
        heroTag: "tambah1",
        backgroundColor: Color.fromRGBO(43, 112, 157, 1),
        child: Icon(Icons.add),
        onPressed: () {
          //namaObat = TextEditingController();
          //dosisObat = TextEditingController();
          controllerNamaObat.add(TextEditingController(text: ''));
          controllerDosisObat.add(TextEditingController(text: ''));
          setState(() {
            countResep = countResep + 1;
          });
        },
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          FloatingActionButton(
            heroTag: "tambah2",
            backgroundColor: Color.fromRGBO(43, 112, 157, 1),
            child: Icon(Icons.add),
            onPressed: () {
              controllerNamaObat.add(TextEditingController(text: ''));
              controllerDosisObat.add(TextEditingController(text: ''));
              setState(() {
                countResep = countResep + 1;
              });
            },
          ),
          FloatingActionButton(
            heroTag: "kurang1",
            backgroundColor: Color.fromRGBO(43, 112, 157, 1),
            child: Icon(Icons.delete),
            onPressed: () {
              controllerNamaObat.removeLast();
              controllerDosisObat.removeLast();
              setState(() {
                countResep = countResep - 1;
              });
            },
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    _load = Loading(context: context);
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Isi Diagnosa"),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          ),
        ),
        body: ListView(
            padding: EdgeInsets.only(left: 20, right: 20),
            children: <Widget>[
              Column(children: <Widget>[
                SizedBox(height: 30),
                Image.network('$link${dataDiagnosa[0]["images_camera"]}'),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.person,
                            color: Color.fromRGBO(43, 112, 157, 1)),
                        onPressed: null,
                        iconSize: 50),
                    Flexible(child: 
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Pasien: ${dataDiagnosa[0]["patient_name"]}",
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18)),
                            Text("Dokter: ${dataDiagnosa[0]["doctor_name"]}",
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18)),
                          ])
                    )
                  ],
                ),
                SizedBox(height: 35),
                Text(
                  "Keluhan",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                SizedBox(height: 5),
                Container(
                  padding: const EdgeInsets.all(15.0),
                  margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(233, 232, 232, 1),
                      border: Border.all(
                        color: Colors.grey,
                      ),
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Text("${dataDiagnosa[0]["keluhan"]}"),
                ),
                SizedBox(height: 30),
                Text(
                  "Diagnosa",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                SizedBox(height: 5),
                Container(
                    margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: TextFormField(
                        controller: controllerNamaPenyakit,
                        decoration: InputDecoration(
                            labelText: 'Masukkan nama penyakit'))),
                SizedBox(height: 10),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: TextField(
                      controller: controllerDiagnosa,
                      minLines: 10,
                      maxLines: 15,
                      autocorrect: false,
                      decoration: InputDecoration(
                        hintText: 'Tulis deskripsi diagnosa anda disini',
                        filled: true,
                        fillColor: Color.fromRGBO(233, 232, 232, 1),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Text(
                  "Resep Obat",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                SizedBox(height: 20),
                ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: countResep,
                    itemBuilder: (context, index) {
                      return fieldIsiObat(index);
                    }),
                buttonTambahKurangObat(countResep),
                SizedBox(height: 30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {
                        if(controllerNamaPenyakit.text.isEmpty && controllerDiagnosa.text.isEmpty){
                          adaDiagnosa = false;
                        }
                        else if(controllerNamaPenyakit.text.isNotEmpty && controllerDiagnosa.text.isNotEmpty){
                          adaDiagnosa = true;
                        }
                        kirimDiagnosa();
                      },
                      color: Color.fromRGBO(43, 112, 157, 1),
                      child: const Text('Kirim Diagnosa',
                          style: TextStyle(color: Colors.white)),
                    ),
                    RaisedButton(
                      onPressed: () {
                        kirimRujukan();
                      },
                      color: Color.fromRGBO(43, 112, 157, 1),
                      child: const Text('Rujuk ke RS',
                          style: TextStyle(color: Colors.white)),
                    ),
                  ],
                ),
                SizedBox(height: 15),
                RaisedButton(
                  onPressed: () {
                    id_penerima = "${dataDiagnosa[0]["patient_code"]}";
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => ChatScreen())
                    );
                  },
                  color: Color.fromRGBO(43, 112, 157, 1),
                  child: const Text('Chat',
                      style: TextStyle(color: Colors.white)),
                ),
                SizedBox(height: 15),
              ])
            ]));
  }
}
