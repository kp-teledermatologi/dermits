import 'package:dermits/Helper/UserHelper.dart';
import 'package:dermits/Model/User.dart';
import 'package:dermits/component/loading.dart';
import 'package:flutter/material.dart';

class UbahPassword extends StatefulWidget {
  @override
  _UbahPasswordState createState() => _UbahPasswordState();
}

class _UbahPasswordState extends State<UbahPassword> {
  final _formKey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final UserHelper _helper = UserHelper();
  User _user;
  Loading _load;

  String _newPassword;
  String _oldPassword;

  void _changePassword() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      try {
        _load.show();
        var result = await _helper.changePassword(
            _oldPassword, _newPassword, _user.idUser);
        _load.hide();

        if (result) {
          popupBerhasil(context);
        }
      } catch (error) {
        _load.hide();
        print(error);
      }
    }
  }

  Future<void> popupBerhasil(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Ubah Password Berhasil !'),
          content: const Text('Data anda telah masuk ke database.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _user = ModalRoute.of(context).settings.arguments;
    _load = Loading(context: context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Ubah Password"),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(8),
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  //old password
                  SizedBox(height: 20),
                  TextFormField(
                    obscureText: true,
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (value) =>
                        FocusScope.of(context).nextFocus(),
                    decoration: InputDecoration(
                      labelText: "Password Lama",
                    ),
                    validator: (value) =>
                        value.isEmpty ? "field ini harus diisi" : null,
                    onSaved: (newValue) {
                      setState(() {
                        _oldPassword = newValue.trim();
                      });
                    },
                  ),

                  //new password
                  SizedBox(height: 20),
                  TextFormField(
                    obscureText: true,
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (value) =>
                        FocusScope.of(context).nextFocus(),
                    decoration: InputDecoration(
                      labelText: "Password Baru",
                    ),
                    controller: _passwordController,
                    validator: (value) =>
                        value.isEmpty ? "field ini harus diisi" : null,
                    onSaved: (newValue) {
                      setState(() {
                        _newPassword = newValue.trim();
                      });
                    },
                  ),

                  //konfirmasi password
                  SizedBox(height: 20),
                  TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: "Konfirmasi Password Baru",
                    ),
                    validator: (value) => value.isEmpty
                        ? "field ini harus diisi"
                        : value != _passwordController.text
                            ? "isi field tidak sama dengan field password"
                            : null,
                  ),

                  //submit btn
                  SizedBox(height: 20),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: RaisedButton(
                      onPressed: _changePassword,
                      textColor: Colors.white,
                      child: Text("Ubah Password"),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
