import 'dart:io';
import 'dart:math';

import 'package:dermits/Helper/UserHelper.dart';
import 'package:dermits/Model/User.dart';
import 'package:dermits/component/loading.dart';
import 'package:dermits/global_var.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UbahFoto extends StatefulWidget {
  @override
  _UbahFotoState createState() => _UbahFotoState();
}

class _UbahFotoState extends State<UbahFoto> {
  final _formKey = GlobalKey<FormState>();
  final _picker = ImagePicker();
  final UserHelper _helper = UserHelper();

  User _user;
  Loading _load;

  File _image;
  bool adaFoto = false;
  String _password;

  Future _getImageCamera() async {
    final pickedFile = await _picker.getImage(source: ImageSource.camera);

    if (pickedFile != null) {
      Navigator.pop(context); //removing picker option dialog

      setState(() {
        _image = File(pickedFile.path);
        adaFoto = true;
      });
    }
  }

  Future _getImageGallery() async {
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      Navigator.pop(context); //removing picker option dialog

      setState(() {
        _image = File(pickedFile.path);
        adaFoto = true;
      });
    }
  }

  void _showPickerOption() {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Pilih Sumber",
                    style: Theme.of(context).textTheme.headline6,
                  ),

                  //divider
                  SizedBox(height: 10),
                  Divider(),
                  SizedBox(height: 10),

                  //option btn
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      //camera btn
                      FlatButton(
                        onPressed: _getImageCamera,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [Icon(Icons.camera_alt), Text("Camera")],
                        ),
                      ),

                      //gallery btn
                      FlatButton(
                        onPressed: _getImageGallery,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(Icons.photo_library),
                            Text("Gallery")
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  void _uploadImg() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      try {
        _load.show();
        var result =
            await _helper.changeProfile(_image, _user.idUser, _password);

        _load.hide();

        if (result) {
          popupBerhasil(context);
        }
      } catch (error) {
        _load.hide();
        print(error);
      }
    }
  }

  Future<void> popupBerhasil(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Ubah Gambar Profile Berhasil !'),
          content: const Text('Data anda telah masuk ke database.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _user = ModalRoute.of(context).settings.arguments;
    _load = Loading(context: context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Ubah Foto Profil"),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(8),
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: MediaQuery.of(context).size.height * .04),

                //profile info
                CircleAvatar(
                  backgroundImage: _image != null
                      ? FileImage(_image)
                      : _user.profilePicture != ""
                          ? NetworkImage(
                              "${link}${_user.profilePicture}?dummy=${Random().nextInt(500)}")
                          : AssetImage("img/avatar_dokter.jpg"),
                  maxRadius: 50,
                ),

                SizedBox(height: 30),
                Container(
                  width: MediaQuery.of(context).size.width * .8,
                  child: OutlineButton(
                    onPressed: _showPickerOption,
                    child: Text("Ambil Gambar"),
                  ),
                ),

                //divider
                SizedBox(height: 20),
                Divider(),
                SizedBox(height: 20),

                //password
                Form(
                  key: _formKey,
                  child: TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(labelText: "Masukkan Password"),
                    validator: (value) => value.isEmpty ? "harus diisi" : null,
                    onSaved: (newValue) {
                      setState(() {
                        _password = newValue;
                      });
                    },
                  ),
                ),

                SizedBox(height: 20),
                Container(
                  width: MediaQuery.of(context).size.width * .8,
                  child: RaisedButton(
                    onPressed: _uploadImg,
                    child: Text('Ubah Gambar'),
                    textColor: Colors.white,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
