import 'dart:math';

import 'package:dermits/Helper/UserHelper.dart';
import 'package:dermits/Model/User.dart';
import 'package:dermits/component/loading.dart';
import 'package:dermits/global_var.dart';
import 'package:flutter/material.dart';

class ManajemenAkun extends StatefulWidget {
  @override
  _ManajemenAkunState createState() => _ManajemenAkunState();
}

class _ManajemenAkunState extends State<ManajemenAkun> {
  User _user = User(
      email: "email",
      name: "name",
      profilePicture: "",
      phone: "phone",
      hospital: "hospital",
      idUser: 12);
  UserHelper helper = UserHelper();
  Loading _load;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      getUser();
    });
  }

  void getUser() async {
    _load.show();

    User tempUser = await helper.getUser(id_user);
    print(tempUser.profilePicture);
    print("${link}${tempUser.profilePicture}");
    setState(() {
      _user = tempUser;
    });

    _load.hide();
  }

  void _toUbahData(BuildContext context) async {
    await Navigator.pushNamed(context, '/ubah_data', arguments: _user);

    getUser();
  }

  void _toUbahFoto(BuildContext context) async {
    await Navigator.pushNamed(context, '/ubah_foto', arguments: _user);

    getUser();
  }

  @override
  Widget build(BuildContext context) {
    _load = Loading(context: context);

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: MediaQuery.of(context).size.height * 0.1),

              //profile info
              CircleAvatar(
                backgroundImage: _user.profilePicture != ""
                    ? NetworkImage(
                        "${link}${_user.profilePicture}?dummy=${Random().nextInt(500)}")
                    : AssetImage("img/avatar_dokter.jpg"),
                maxRadius: 50,
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: FittedBox(
                  child: Text(
                    _user.name,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: FittedBox(
                  child: Text(
                    _user.email,
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: FittedBox(
                  child: Text(
                    "Telp. ${_user.phone}",
                    style: TextStyle(color: Colors.grey),
                  ),
                ),
              ),

              SizedBox(height: MediaQuery.of(context).size.height * 0.05),

              //menu
              Column(
                children: ListTile.divideTiles(context: context, tiles: [
                  ListTile(
                    onTap: () {
                      Navigator.pushNamed(context, '/ubah_password',
                          arguments: _user);
                    },
                    title: Text(
                      "Ubah Password",
                    ),
                    trailing: Icon(Icons.keyboard_arrow_right),
                  ),
                  ListTile(
                    onTap: () {
                      _toUbahData(context);
                    },
                    title: Text(
                      "Ubah Data Pengguna",
                    ),
                    trailing: Icon(Icons.keyboard_arrow_right),
                  ),
                  ListTile(
                    onTap: () {
                      _toUbahFoto(context);
                    },
                    title: Text(
                      "Ubah Foto Profile",
                    ),
                    trailing: Icon(Icons.keyboard_arrow_right),
                  ),
                  ListTile(
                    onTap: () {
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/login', (route) => false);
                    },
                    title: Text(
                      "Logout",
                      style: TextStyle(color: Colors.red),
                    ),
                    trailing: Icon(Icons.exit_to_app),
                  ),
                ]).toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
