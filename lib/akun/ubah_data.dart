import 'package:dermits/Helper/UserHelper.dart';
import 'package:dermits/Model/User.dart';
import 'package:dermits/component/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class UbahData extends StatefulWidget {
  @override
  _UbahDataState createState() => _UbahDataState();
}

class _UbahDataState extends State<UbahData> {
  final _formKey = GlobalKey<FormState>();
  final UserHelper _helper = UserHelper();
  final _textControl = TextEditingController();

  User _user;

  String _password;
  String _name;
  String _email;
  String _phone;
  String _username;
  String _dob;

  Loading _load;

  void _ubahData() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      try {
        _load.show();
        var result = await _helper.changeData(
            _user.idUser, _password, _name, _email, _phone, _username, _dob);
        _load.hide();

        if (result) {
          setState(() {
            _user.name = _name;
            _user.email = _email;
            _user.phone = _phone;
            _user.username = _username;
            _user.dob = _dob;
          });
          popupBerhasil(context);
        }
      } catch (error) {
        _load.hide();
        print(error);
      }
    }
  }

  void _showDatePicker(context) async {
    DateTime now = DateTime.now();
    DateFormat format = DateFormat("yyyy-MM-dd");

    //hide keyboard from appearing
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    FocusScope.of(context).unfocus();

    DateTime date = await showDatePicker(
        context: context,
        initialDate: now,
        firstDate: DateTime(1700),
        lastDate: now);

    if (date != null) {
      _textControl.text = format.format(date);
      FocusScope.of(context).nextFocus();
      setState(() {
        _dob = format.format(date);
      });
    }
  }

  Future<void> popupBerhasil(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Ubah Data Berhasil !'),
          content: const Text('Data anda telah masuk ke database.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop(_user);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      _textControl.text = _user.dob;
    });
  }

  @override
  Widget build(BuildContext context) {
    _user = ModalRoute.of(context).settings.arguments;
    _load = Loading(context: context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Ubah Data"),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(8),
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  //nama
                  SizedBox(height: 20),
                  TextFormField(
                    decoration: InputDecoration(labelText: "Nama"),
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (value) =>
                        FocusScope.of(context).nextFocus(),
                    validator: (value) => value.isEmpty ? "harus diisi" : null,
                    initialValue: _user.name,
                    onSaved: (newValue) {
                      setState(() {
                        _name = newValue;
                      });
                    },
                  ),

                  //email
                  SizedBox(height: 20),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (value) =>
                        FocusScope.of(context).nextFocus(),
                    decoration: InputDecoration(labelText: "Email"),
                    validator: (value) => value.isEmpty ? "harus diisi" : null,
                    initialValue: _user.email,
                    onSaved: (newValue) {
                      setState(() {
                        _email = newValue;
                      });
                    },
                  ),

                  //telp
                  SizedBox(height: 20),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (value) =>
                        FocusScope.of(context).nextFocus(),
                    decoration: InputDecoration(labelText: "Telp"),
                    validator: (value) => value.isEmpty ? "harus diisi" : null,
                    initialValue: _user.phone,
                    onSaved: (newValue) {
                      setState(() {
                        _phone = newValue;
                      });
                    },
                  ),

                  //username
                  SizedBox(height: 20),
                  TextFormField(
                    decoration: InputDecoration(labelText: "Username"),
                    validator: (value) => value.isEmpty ? "harus diisi" : null,
                    initialValue: _user.username,
                    onSaved: (newValue) {
                      setState(() {
                        _username = newValue;
                      });
                    },
                  ),

                  //dob
                  SizedBox(height: 20),
                  TextFormField(
                    controller: _textControl,
                    decoration: InputDecoration(labelText: "Tanggal Lahir"),
                    validator: (value) => value.isEmpty ? "harus diisi" : null,
                    onTap: () => _showDatePicker(context),
                  ),

                  //divider
                  SizedBox(height: 20),
                  Divider(color: Colors.grey[800]),

                  //password
                  SizedBox(height: 20),
                  TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(labelText: "Masukkan Password"),
                    validator: (value) => value.isEmpty ? "harus diisi" : null,
                    onSaved: (newValue) {
                      setState(() {
                        _password = newValue;
                      });
                    },
                  ),

                  //button
                  SizedBox(height: 20),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: RaisedButton(
                      onPressed: _ubahData,
                      textColor: Colors.white,
                      child: Text("Ubah Data"),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
