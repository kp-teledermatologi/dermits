import 'package:flutter/material.dart';
import 'package:dermits/homepage_user.dart';
import 'package:dermits/homepage_dokter.dart';
import 'package:dermits/SignUp.dart';
import 'package:http/http.dart' as http;
import 'package:dermits/global_var.dart';
import 'dart:convert';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';
//import 'package:flutter/services.dart';

class Login extends StatefulWidget {
  @override
  _Login createState() => _Login();
}

enum LoginStatus { offline, online }

class _Login extends State<Login> {
  void initState() {
    super.initState();
  }

  final _key = new GlobalKey<FormState>();
  String email, password;
  List dataUser = [];

  bool _secureText = true;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  check() {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      print("sedang login");
      login();
    }
  }

  login() async {
    var url = link + "login.php";
    var response =
        await http.post(url, body: {"email": email, "password": password});
    final data = jsonDecode(response.body);
    print(data);
    String value = data['value'];
    String pesan = data['message'];
    String roleBaru = data['role'].toString();
    String id = data['id_user'].toString();
    String usernameBaru = data['username'].toString();
    String nameBaru = data['name'].toString();

    if (value == "1") {
      Navigator.pushReplacementNamed(context, '/root_page');
      id_user = id;
      role = roleBaru;
      username = usernameBaru;
      name = nameBaru;

      print(pesan);
    } else {
      print(pesan);
      Fluttertoast.showToast(msg: "login gagal");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: Color.fromRGBO(251, 120, 19, 1),
        body:
            ListView(padding: EdgeInsets.only(left: 60, right: 60), children: <
                Widget>[
      Column(children: <Widget>[
        SizedBox(height: 200),
        Image(image: AssetImage("img/logo.png")),
        SizedBox(height: 70),
        Container(
          child: Form(
              key: _key,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    validator: (tulisemail) {
                      if (tulisemail.isEmpty) {
                        return "masukkan email";
                      }
                    },
                    onSaved: (tulisemail) => email = tulisemail,
                    decoration: InputDecoration(
                      hintText: 'Email',
                      hintStyle: new TextStyle(
                          color: Color.fromRGBO(43, 112, 157, 1),
                          fontSize: 16.0),
                      filled: true,
                      fillColor: Color.fromRGBO(233, 232, 232, 1),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                    ),
                  ),
                  SizedBox(height: 15),
                  TextFormField(
                    validator: (pass) {
                      if (pass.isEmpty) {
                        return "masukkan password";
                      }
                    },
                    onSaved: (pass) => password = pass,
                    obscureText: _secureText,
                    decoration: InputDecoration(
                      hintText: 'Password',
                      suffixIcon: IconButton(
                        onPressed: showHide,
                        icon: Icon(_secureText
                            ? Icons.visibility_off
                            : Icons.visibility),
                      ),
                      hintStyle: new TextStyle(
                          color: Color.fromRGBO(43, 112, 157, 1),
                          fontSize: 16.0),
                      filled: true,
                      fillColor: Color.fromRGBO(233, 232, 232, 1),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        borderSide:
                            BorderSide(color: Color.fromRGBO(43, 112, 157, 1)),
                      ),
                    ),
                  ),
                ],
              )),
        ),
        SizedBox(height: 15),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                check();
              },
              color: Color.fromRGBO(43, 112, 157, 1),
              child: const Text('Sign In',
                  style: TextStyle(fontSize: 20, color: Colors.white)),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => SignUp()));
              },
              color: Color.fromRGBO(43, 112, 157, 1),
              child: const Text('Sign Up',
                  style: TextStyle(fontSize: 20, color: Colors.white)),
            ),
          ],
        ),
        SizedBox(height: 15),
      ])
    ]));
  }
}
