import 'package:dermits/component/display_name.dart';
import 'package:flutter/material.dart';

class UpdateCardRujukan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            DisplayName(
              name: "Dr. Tumiyem",
              title: "Dokter Umum Klinik Tong Fang",
              profilePhoto: AssetImage("img/avatar_dokter.jpg"),
            ),
            SizedBox(height: 10),
            AspectRatio(
              aspectRatio: 16 / 5,
              child: Image.asset(
                "img/placeholder.png",
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(height: 10),
            Text("Sebaiknya dirujuk di"),
            SizedBox(height: 10),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * .2,
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                border: Border.all(width: 1),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //rs name
                  FittedBox(
                    child: Text(
                      "Rs. Sehat Selamanya",
                      softWrap: true,
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(color: Theme.of(context).accentColor),
                    ),
                  ),

                  //rs address
                  SizedBox(height: 10),
                  Text(
                    "Jl. Hilang Selamanya No. 124, Surabaya, Jawa Timur, 1366",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(color: Colors.grey),
                  ),

                  SizedBox(height: 10),
                  FittedBox(
                    child: Text(
                      "Telp. 081 - 1234 - 5666",
                      style: Theme.of(context)
                          .textTheme
                          .bodyText2
                          .copyWith(color: Colors.grey),
                    ),
                  ),

                  Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      height: 20,
                      child: MaterialButton(
                          onPressed: () {},
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                "lihat di peta",
                                style: TextStyle(
                                    color: Theme.of(context).accentColor),
                              ),
                              Icon(
                                Icons.keyboard_arrow_right,
                                color: Theme.of(context).accentColor,
                              )
                            ],
                          )),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
