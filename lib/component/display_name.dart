import 'package:flutter/material.dart';

class DisplayName extends StatelessWidget {
  DisplayName({Key key, this.name, this.title, this.profilePhoto})
      : super(key: key);

  final String name;
  final String title;
  final ImageProvider profilePhoto;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        profilePhoto != null
            ? CircleAvatar(
                backgroundImage: profilePhoto,
              )
            : SizedBox.shrink(),
        SizedBox(width: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * .7,
              child: Text(
                '$name',
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(color: Theme.of(context).accentColor),
              ),
            ),
            title != null
                ? Text(
                    '$title',
                    style: Theme.of(context)
                        .textTheme
                        .bodyText2
                        .copyWith(color: Colors.grey),
                  )
                : SizedBox.shrink(),
          ],
        ),
      ],
    );
  }
}
