import 'package:dermits/Model/Diagnosis.dart';
import 'package:dermits/Model/User.dart';
import 'package:dermits/component/display_name.dart';
import 'package:dermits/global_var.dart';
import 'package:flutter/material.dart';

class UpdateCardDiagnosa extends StatelessWidget {
  UpdateCardDiagnosa({Key key, this.diagnosis}) : super(key: key);

  final Diagnosis diagnosis;

  void _toDetails(BuildContext context) {
    pilihIdExamination = diagnosis.idDiagnosis;

    Navigator.pushNamed(context, '/lihat_diagnosa');
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //display name dokter
            DisplayName(
              name: "${diagnosis.user.name}",
              title: "Dokter di ${diagnosis.user.hospital}",
              profilePhoto:
                  NetworkImage("${link}${diagnosis.user.profilePicture}"),
            ),
            SizedBox(height: 10),

            //gambar keluhan
            AspectRatio(
              aspectRatio: 16 / 5,
              child: Image.network(
                "${link}${diagnosis.imageCamera}",
                fit: BoxFit.cover,
              ),
            ),

            //title
            SizedBox(height: 10),
            Text("Diagnosa"),

            //box diagnosa
            SizedBox(height: 10),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * .2,
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                border: Border.all(width: 1),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    diagnosis.disease,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  SizedBox(height: 10),
                  Text(diagnosis.description)
                ],
              ),
            ),

            //action btn
            SizedBox(height: 10),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                height: 20,
                child: MaterialButton(
                  onPressed: () {
                    _toDetails(context);
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        "Lihat Detail",
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      Icon(
                        Icons.keyboard_arrow_right,
                        color: Theme.of(context).accentColor,
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
