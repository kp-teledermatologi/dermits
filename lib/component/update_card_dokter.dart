import 'package:dermits/Model/Diagnosis.dart';
import 'package:dermits/component/display_name.dart';
import 'package:dermits/global_var.dart';
import 'package:flutter/material.dart';

class UpdateCardDokter extends StatelessWidget {
  UpdateCardDokter({Key key, this.diagnosis}) : super(key: key);

  final Diagnosis diagnosis;

  void _toIsi(BuildContext context) {
    pilihIdExamination = diagnosis.idDiagnosis;

    Navigator.pushNamed(context, '/isi_diagnosa');
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            DisplayName(
              name: "${diagnosis.user.name}",
              profilePhoto:
                  NetworkImage("${link}${diagnosis.user.profilePicture}"),
            ),
            SizedBox(height: 10),

            //gambar keluhan
            AspectRatio(
              aspectRatio: 16 / 5,
              child: Image.network(
                link + diagnosis.imageCamera,
                fit: BoxFit.cover,
              ),
            ),

            //title
            SizedBox(height: 10),
            Text("Keluhan"),

            //keluhan box
            SizedBox(height: 10),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * .2,
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                border: Border.all(width: 1),
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(diagnosis.keluhan),
            ),

            //box to diagnosa
            SizedBox(height: 10),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                height: 20,
                child: MaterialButton(
                  onPressed: () {
                    _toIsi(context);
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        "Beri Diagnosa",
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      Icon(
                        Icons.keyboard_arrow_right,
                        color: Theme.of(context).accentColor,
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
