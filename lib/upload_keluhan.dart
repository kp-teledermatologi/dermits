import 'package:dermits/rootPage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:dermits/global_var.dart';
import 'package:intl/intl.dart';

class UploadKeluhan extends StatefulWidget {
  @override
  _UploadKeluhan createState() => _UploadKeluhan();
}

class _UploadKeluhan extends State<UploadKeluhan> {
  File _image;
  bool adaFoto = false;
  final picker = ImagePicker();
  var respon;
  final controllerKeluhan = TextEditingController();
  bool adaTeksKeluhan = false;

  Future getImageCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      _image = File(pickedFile.path);
      adaFoto = true;
    });
  }

  Future getImageGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      _image = File(pickedFile.path);
      adaFoto = true;
    });
  }

  Widget tampilkanFoto() {
    if (adaFoto == false) {
      return Image(image: AssetImage('img/placeholder.png'));
    } else {
      return Image.file(_image);
    }
  }

  Future<void> popupBerhasil(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Upload Keluhan Berhasil'),
          content: const Text('Data anda telah masuk ke database.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RootPage()),
                );
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> popupGagal(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Upload Keluhan Gagal'),
          content: const Text('Data tidak terkirim ke database.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RootPage()),
                );
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> popupTeksKosong(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Upload Keluhan Gagal'),
          content: const Text('Teks keluhan belum diisi.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> popupFotoKosong(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Upload Keluhan Gagal'),
          content: const Text('Foto belum dipilih.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void kirimKeluhan() async {
    if(adaTeksKeluhan && adaFoto) {
      var base64img = base64Encode(_image.readAsBytesSync());
      var sekarang = DateTime.now();
      String tanggal = DateFormat('dd-MM-yyyy').format(sekarang);
      var fileName = '${id_user}_${tanggal}.jpg';
      var url = "${link}img/upload_keluhan.php";
      var response = await http.post(url, body: {
        "image": base64img,
        "images_camera": fileName,
        "keluhan": controllerKeluhan.text,
        "patient_name": name,
        "patient_code": id_user,
      });

      respon = jsonDecode(response.body);
      if (respon["value"] == '1') {
        popupBerhasil(context);
      } else if (respon["value"] == '0') {
        popupGagal(context);
      }
    }
    else if (!adaFoto){
      popupFotoKosong(context);
    }
    else if (!adaTeksKeluhan){
      popupTeksKosong(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Upload Keluhan"),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          ),
        ),
        body: ListView(
            padding: EdgeInsets.only(left: 20, right: 20),
            children: <Widget>[
              Column(children: <Widget>[
                SizedBox(height: 30),
                tampilkanFoto(),
                SizedBox(height: 20),
                Text(
                  "Pilih foto",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.camera_alt,
                            color: Color.fromRGBO(43, 112, 157, 1)),
                        onPressed: getImageCamera,
                        iconSize: 40),
                    IconButton(
                        icon: Icon(Icons.image,
                            color: Color.fromRGBO(43, 112, 157, 1)),
                        onPressed: getImageGallery,
                        iconSize: 40),
                  ],
                ),
                SizedBox(height: 35),
                Text(
                  "Keluhan",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                SizedBox(height: 5),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: TextField(
                      controller: controllerKeluhan,
                      minLines: 10,
                      maxLines: 15,
                      autocorrect: false,
                      decoration: InputDecoration(
                        hintText: 'Tulis keluhan anda disini',
                        filled: true,
                        fillColor: Color.fromRGBO(233, 232, 232, 1),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                      ),
                    ),
                  ),
                ),
                RaisedButton(
                  onPressed: () {
                    controllerKeluhan.text.isEmpty ? adaTeksKeluhan = false : adaTeksKeluhan = true;
                    kirimKeluhan();
                  },
                  color: Color.fromRGBO(43, 112, 157, 1),
                  child: const Text('Kirim',
                      style: TextStyle(color: Colors.white)),
                ),
                SizedBox(height: 15),
              ])
            ]));
  }
}
