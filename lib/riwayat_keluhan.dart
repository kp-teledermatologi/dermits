import 'package:dermits/global_var.dart';
import 'package:dermits/isi_diagnosa.dart';
import 'package:dermits/lihat_diagnosa.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:dermits/component/loading.dart';

class RiwayatKeluhan extends StatefulWidget {
  @override
  _RiwayatKeluhan createState() => _RiwayatKeluhan();
}

class _RiwayatKeluhan extends State<RiwayatKeluhan> {
  Loading _load;

  @override
  void initState() {
    super.initState();
    initializeDateFormatting("id");
    Future.delayed(Duration.zero, () {
      ambilDataRiwayat();
    });
  }

  List dataRiwayat = [];

  void ambilDataRiwayat() async {
    _load.show();
    var response;
    if (role == '0' || role == null) {
      var url = "${link}riwayat_keluhan.php";
      response = await http.post(url, body: {
        "patient_code": "${id_user}",
      });
      print("ambil data");
    } else {
      var url = "${link}riwayat_diagnosa.php";
      response = await http.post(url, body: {
        "doctor_username": "${username}",
      });
    }

    setState(() {
      dataRiwayat = json.decode(response.body);
      print(dataRiwayat);
    });
    _load.hide();
  }

  String tanggalRiwayat(int index, String pilih) {
    String tanggalBelumFormat = dataRiwayat[index]["createdAt"];
    DateTime tanggalFormat = DateTime.parse(tanggalBelumFormat);

    if (pilih == "tanggal") {
      final DateFormat formatter = DateFormat.yMMMMd('id');
      final String formatted = formatter.format(tanggalFormat);
      return formatted;
    } else if (pilih == "hari") {
      final DateFormat formatter = DateFormat.EEEE('id');
      final String formatted = formatter.format(tanggalFormat);
      return formatted;
    }
  }

  Widget listRiwayatKeluhan(BuildContext context, int index) {
    return GestureDetector(
      onTap: () {
        pilihIdExamination = "${dataRiwayat[index]["id_examinations"]}";
        if (role != '0') {
          checked = "${dataRiwayat[index]["checked"]}";
          if (checked == '0') {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => IsiDiagnosa()));
          } else {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => LihatDiagnosa()));
          }
        } else {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => LihatDiagnosa()));
        }
      },
      child: Card(
          child: Container(
        padding: const EdgeInsets.all(20.0),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
          CircleAvatar(
            backgroundImage:
                NetworkImage('${link}${dataRiwayat[index]["profilePicture"]}'),
            minRadius: 40,
          ),
          Flexible(
              child: Container(
                  padding: const EdgeInsets.only(left: 40.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(tanggalRiwayat(index, "hari"),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18)),
                        SizedBox(height: 5),
                        Text(tanggalRiwayat(index, "tanggal"),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18)),
                        SizedBox(height: 10),
                        role == "0" ?
                        Text('${dataRiwayat[index]["doctor_name"]}',
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 14))
                        :
                        Text('${dataRiwayat[index]["patient_name"]}',
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 14)),
                        SizedBox(height: 5),
                        int.parse(dataRiwayat[index]["checked"]) <= 1
                            ? Text('${dataRiwayat[index]["clinic_name"]}',
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontSize: 14))
                            : Text('${dataRiwayat[index]["doctor_hospital"]}',
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontSize: 14))
                      ])))
        ]),
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    _load = Loading(context: context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: role == '0' ? Text("Riwayat Keluhan") : Text("Riwayat Diagnosa"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, false),
        ),
      ),
      body: ListView.builder(
          padding: EdgeInsets.only(left: 20, right: 20),
          shrinkWrap: true,
          itemCount: dataRiwayat == null ? 0 : dataRiwayat.length,
          itemBuilder: (BuildContext context, int index) =>
              listRiwayatKeluhan(context, index)),
    );
  }
}
